#!/bin/bash

python3 train.py --cfg yolov3-spp.cfg --batch-size 4  --accumulate 1 --data data/coco16_2014.data --weights 'weights/yolov3-spp-ultralytics.pt' --img-size 320 --epoch 1000
