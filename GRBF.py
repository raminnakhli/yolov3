from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import time
import os
import copy
from tqdm import tqdm 
import random

plt.ion()   # interactive mode

import numpy as np
import torch
from torch import nn
from torch.nn.modules.loss import _Loss
import torch.nn.functional as F
#from utils.utils import BATCH_NORM
EXP = True
PAPER_OUT = False
SOFT_MAX = False
RELU = False
BATCH_NORM_BF = True
BATCH_NORM_AF = False
LAYER_NORM_AF = False
LAMBDA_EVAL = 30

class GRBFLayer(nn.Module):
    """
    GRBF layer for classification
    """

    def __init__(self, num_in_feature, num_shared_feature, num_out_feature, num_classes, p):
        super(GRBFLayer, self).__init__()
        self.num_classes = num_classes
        self.num_out_feature = num_out_feature
        self.shared_fc = nn.Identity()
        if num_shared_feature != 0:
            self.shared_fc = nn.Linear(num_in_feature, num_shared_feature, bias=False)
            torch.nn.init.xavier_normal_(self.shared_fc.weight)
            self.add_module('Linear_Shared', self.shared_fc)
        else:
            num_shared_feature = num_in_feature
        self.class_fc = nn.Linear(num_shared_feature, num_out_feature * num_classes)
        torch.nn.init.xavier_normal_(self.class_fc.weight)
        self.add_module('Linear_Specific', self.class_fc)
#        self.distance_function = lambda x: torch.norm(x, p=p, dim=-1).pow(p)
        if p == 1:
            self.distance_function = lambda x: torch.sum(torch.abs(x), dim=-1)
        elif p == 2:
            self.distance_function = lambda x: torch.sum(x.pow(2), dim=-1)
        else:
            raise ValueError("p must be 1 or 2")
        self.bn_bf = torch.nn.BatchNorm1d(num_in_feature)
#        self.bn_bf = torch.nn.BatchNorm1d(num_shared_feature)
        self.bn_af = torch.nn.BatchNorm1d(self.num_classes)
        self.dummy_fc = nn.Linear(num_in_feature, num_in_feature)
        self.layer_norm = torch.nn.LayerNorm(self.num_classes)

    def forward(self, x):
        if RELU:
            x = F.relu(self.dummy_fc(x))
        if BATCH_NORM_BF:
            size = x.size()
            x = self.bn_bf(x.view(-1, size[-1]))
            x = x.view(size)
        x = self.shared_fc(x)
        dim_len = len(x.size())
        x = self.class_fc(x).view(*x.size()[:dim_len - 1], self.num_classes, self.num_out_feature)
        x = self.distance_function(x).squeeze(dim=-1) #original
        if BATCH_NORM_AF:
            size = x.size()
            x = self.bn_af(x.view(-1, self.num_classes))
            x = x.view(size)
        if LAYER_NORM_AF:
            x = self.layer_norm(x)
        if SOFT_MAX:
            return -x, torch.nn.functional.softmax(-x, dim=-1)
        if PAPER_OUT:
            h = LAMBDA_EVAL - x
            h = -torch.nn.functional.logsigmoid(-h)
            return -x, torch.exp(-x + h - torch.sum(h, dim=-1, keepdim=True))
        if EXP:
            y = x - torch.min(x, dim=-1, keepdim=True)[0]
            return -x, torch.exp(-y)
        return -x, -x # otiginal


class SoftML(_Loss):
    """
    SoftML loss function for GRBF model

    Shape:
        - Input: :math:`(N, C)` where `C = number of classes`, or
          :math:`(N, C, d_1, d_2, ..., d_K)` with :math:`K \geq 1`
          in the case of `K`-dimensional loss.
        - Target: :math:`(N)` where each value is :math:`0 \leq \text{targets}[i] \leq C-1`, or
          :math:`(N, d_1, d_2, ..., d_K)` with :math:`K \geq 1` in the case of
          K-dimensional loss.
        - Output: scalar.
          If :attr:`reduction` is ``'none'``, then the same size as the target: :math:`(N)`, or
          :math:`(N, d_1, d_2, ..., d_K)` with :math:`K \geq 1` in the case
          of K-dimensional loss.

    """

    def __init__(self, misclassified_threshold, rejection=False, weight=None, size_average=None, ignore_index=-100,
                 reduce=None, reduction='mean'):
        super(SoftML, self).__init__(size_average=size_average)
        self.nll_loss = nn.NLLLoss(weight=weight, size_average=size_average, ignore_index=ignore_index, reduce=reduce,
                                   reduction=reduction)
        self.misclassified_threshold = misclassified_threshold
        self.rejection = rejection
        # todo: add rejection

    def forward(self, x, target):
        exp_max_value = 30
        exp_min_value = -300

        # calculate likelihood matrix
        likelihood_matrix = self.misclassified_threshold + x
        likelihood_matrix[likelihood_matrix < exp_min_value] = exp_min_value
        max_indices = likelihood_matrix < exp_max_value
        likelihood_matrix[max_indices] = torch.log(1 + torch.exp(likelihood_matrix[max_indices]))
        likelihood_matrix = x + likelihood_matrix - torch.sum(likelihood_matrix, dim=1, keepdim=True)

        return self.nll_loss(likelihood_matrix, target)


class SoftDML(_Loss):

    def __init__(self, lambdaMax, lambdaMin, c=1):
        super(SoftDML, self).__init__(size_average=None)
        self.lambdaMax = lambdaMax
        self.lambdaMin = lambdaMin
        self.c = c

    def forward(self, x, target):

        distance = -x
        correct_index = torch.zeros_like(x).scatter(1, target.unsqueeze(1), 1)
        incorrect_index = torch.ones_like(x) - correct_index

        hMin = distance * correct_index - self.lambdaMin
        hMax = self.lambdaMax - distance * incorrect_index
        hMax = -torch.nn.functional.logsigmoid(-hMax)
        hMin = -torch.nn.functional.logsigmoid(-hMin)
        hMax = smooth_maximum(hMax, 1, scale=1)
        return smooth_l1_loss(hMax) + self.c * smooth_l1_loss(hMin)
#        return torch.sum(hMax)+torch.sum(hMin)



class HardDML(_Loss):

    def __init__(self, lambdaMax, lambdaMin, c):
        super(HardDML, self).__init__(size_average=None)
        self.lambdaMax = lambdaMax
        self.lambdaMin = lambdaMin
        self.c = c

    def forward(self, x, target):

        distance = -x
        correct_index = torch.zeros_like(x).scatter(-1, target.data.unsqueeze(1), 1)
        incorrect_index = torch.ones_like(x) - correct_index

        hMin = distance * correct_index - self.lambdaMin
        hMax = self.lambdaMax - distance * incorrect_index
        hMax = hMax.clamp(min=0)
        hMin = hMin.clamp(min=0)
        hMax = smooth_maximum(hMax, 1, scale=1)
        return smooth_l1_loss(hMax) + self.c * smooth_l1_loss(hMin)
#        return torch.sum(hMax)+torch.sum(hMin)

class RejectionLoss(_Loss):

    def __init__(self, lambdaMax):
        super(RejectionLoss, self).__init__(size_average=None)
        self.lambdaMax = lambdaMax

    def forward(self, x, b, a, gi, gj):
        distance = -x
        loss = self.lambdaMax - distance
        loss[b, a, gi, gj] = 0
        loss = -torch.nn.functional.logsigmoid(-loss)
        return smooth_l1_loss(loss)

class SoftDDML(_Loss):
    
    def __init__(self, lambdaMax, lambdaMin, c = 1):
        super(SoftDDML, self).__init__(size_average=None)
        self.lambdaMax = lambdaMax
        self.lambdaMin = lambdaMin
        self.c = c

    def forward(self, x, target):
#        if EXP == False:
        if True:
            distance = -x
            N = target.size(0) # get target size
            correct_class_distance = distance[torch.arange(0, N).long(), target.data].view(-1,1) # extract correct class distance
            multi_class_svm_loss = self.lambdaMax - (distance - correct_class_distance) # calculate loss of misclassifed classes with a margin
            multi_class_svm_loss[torch.arange(0, N).long(), target.data] = 0 # remove loss for the correct class
            multi_class_svm_loss = -torch.nn.functional.logsigmoid(-multi_class_svm_loss)
            correct_class_loss = correct_class_distance - self.lambdaMin # calculate correct class loss
            correct_class_loss = -torch.nn.functional.logsigmoid(-correct_class_loss)
            return smooth_l1_loss(multi_class_svm_loss) + self.c * smooth_l1_loss(correct_class_loss)
#        else:
#            distance = -torch.log(x)
#            N = target.size(0) # get target size
#            correct_class_distance = distance[torch.arange(0, N).long(), target.data].view(-1,1) # extract correct class distance
#            multi_class_svm_loss = self.lambdaMax - (distance - correct_class_distance) # calculate loss of misclassifed classes with a margin
#            multi_class_svm_loss[torch.arange(0, N).long(), target.data] = 0 # remove loss for the correct class
#            multi_class_svm_loss = -torch.nn.functional.logsigmoid(-multi_class_svm_loss)
#            correct_class_loss = correct_class_distance - self.lambdaMin # calculate correct class loss
#            correct_class_loss = -torch.nn.functional.logsigmoid(-correct_class_loss)
#            return smooth_l1_loss(multi_class_svm_loss) + self.c * smooth_l1_loss(correct_class_loss)

#            N = target.size(0) # get target size
#            correct_class_distance = x[torch.arange(0, N).long(), target.data].view(-1,1) # extract correct class distance
#            multi_class_svm_loss = self.lambdaMax + torch.log(x - correct_class_distance)
#            multi_class_svm_loss[torch.arange(0, N).long(), target.data] = 0 # remove loss for the correct class
#            multi_class_svm_loss = -torch.nn.functional.logsigmoid(-multi_class_svm_loss)
#            correct_class_loss = -correct_class_distance - self.lambdaMin # calculate correct class loss
#            correct_class_loss = -torch.nn.functional.logsigmoid(-correct_class_loss)
#            return smooth_l1_loss(multi_class_svm_loss) + self.c * smooth_l1_loss(correct_class_loss)


class HardDDML(_Loss):

    def __init__(self, lambdaMax, lambdaMin, c = 1):
        super(HardDDML, self).__init__(size_average=None)
        self.lambdaMax = lambdaMax
        self.lambdaMin = lambdaMin
        self.c = c

    def forward(self, x, target):
        distance = -x
        N = target.size(0) # get target size
        correct_class_distance = distance[torch.arange(0, N).long(), target.data].view(-1,1) # extract correct class distance
        multi_class_svm_loss = self.lambdaMax - (distance - correct_class_distance) # calculate loss of misclassifed classes with a margin
        multi_class_svm_loss[torch.arange(0, N).long(), target.data] = 0 # remove loss for the correct class
        multi_class_svm_loss = multi_class_svm_loss.clamp(min=0) # remove losses with enough loss margin
        correct_class_loss = correct_class_distance - self.lambdaMin # calculate correct class loss
        correct_class_loss = correct_class_loss.clamp(min=0)
        return smooth_l1_loss(multi_class_svm_loss) + self.c * smooth_l1_loss(correct_class_loss)


class SoftPDDML(_Loss):

    def __init__(self, lambdaMax, lambdaMin, c = 1):
        super(SoftPDDML, self).__init__(size_average=None)
        self.lambdaMax = lambdaMax
        self.lambdaMin = lambdaMin
        self.c = c

    def forward(self, x, target):
        distance = -x
        N = target.size(0) # get target size
        correct_distance = distance[torch.arange(0, N).long(), target.data].view(-1,1) # extract correct class distance
        multi_class_svm_loss = self.lambdaMax  - (distance - correct_distance) # calculate loss of misclassifed classes with a margin
        multi_class_svm_loss[torch.arange(0, N).long(), target.data] = 0 # remove loss for the correct class
        multi_class_svm_loss = -torch.nn.functional.logsigmoid(-multi_class_svm_loss)
        multi_class_svm_loss = smooth_maximum(multi_class_svm_loss, 1, scale=1) # get maximum loss of all the losses
        correct_class_loss = correct_distance - self.lambdaMin # calculate correct class loss
        correct_class_loss = -torch.nn.functional.logsigmoid(-correct_class_loss)
        return smooth_l1_loss(multi_class_svm_loss) + self.c * smooth_l1_loss(correct_class_loss)


class HardPDDML(_Loss):

    def __init__(self, lambdaMax, lambdaMin, c = 1):
        super(HardPDDML, self).__init__(size_average=None)
        self.lambdaMax = lambdaMax
        self.lambdaMin = lambdaMin
        self.c = c

    def forward(self, x, target):
        distance = -x
        N = target.size(0) # get target size
        correct_distance = distance[torch.arange(0, N).long(), target.data].view(-1,1) # extract correct class distance
        multi_class_svm_loss = self.lambdaMax  - (distance - correct_distance) # calculate loss of misclassifed classes with a margin
        multi_class_svm_loss[torch.arange(0, N).long(), target.data] = 0 # remove loss for the correct class
        multi_class_svm_loss = multi_class_svm_loss.clamp(min=0) # remove losses with enough loss margin
        multi_class_svm_loss = smooth_maximum(multi_class_svm_loss, 1, scale=1) # get maximum loss of all the losses
        correct_class_loss = correct_distance - self.lambdaMin # calculate correct class loss
        correct_class_loss = correct_class_loss.clamp(min=0) # remove losses with enough margin
        return smooth_l1_loss(multi_class_svm_loss) + self.c * smooth_l1_loss(correct_class_loss)


def smooth_maximum(value, dim, scale = 100):
    return torch.logsumexp(value * scale, dim=dim) / scale


def smooth_l1_loss(value, beta = 100, size_average=True):
    """
    very similar to the smooth_l1_loss from pytorch, but with
    the extra beta parameter
    """
    n = torch.abs(value)
    cond = n < beta
    loss = torch.where(cond, 0.5 * n * 2 / beta, n - 0.5 * beta)
    if size_average:
        return loss.mean()
    return loss.sum()

